from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.utils.functional import curry
from .models import Articles
from .models import Category
from django import forms


class AddArticleForm(forms.ModelForm):

    class Meta:
        model = Articles
        exclude = ['author']


class AddCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        exclude = ['author']


class EditorAdmin(admin.ModelAdmin):

    form = AddArticleForm
    list_display = ('title', 'category', 'author', 'publish', 'published')


    def get_readonly_fields(self, request, obj=None):
        read_only = ('publish',)

        if not request.user.has_perm('articles.can_publish_art'):
            read_only += ('published',)
        if obj is not None:
            if obj.author != request.user or obj.published == True:  # In edit mode
                read_only += ('title', 'body', 'category',)

        return read_only + self.readonly_fields

    def history_view(self, request, object_id, extra_context=None):
        if not request.user.is_superuser:
            raise PermissionDenied
        super().history_view(request,object_id,extra_context)


    def save_model(self, request, obj, form, change):

        if not change:
            obj.author = request.user
        obj.save()


class CategoryAdmin(admin.ModelAdmin):

    form = AddArticleForm
    list_display = ('title', 'author',)

    def get_queryset(self, request):
        qs = super(admin.ModelAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author_id=request.user)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'user', None) is None:
            obj.user = request.user
            obj.author = request.user
        obj.save()

    def has_change_permission(self, request, obj=None):
        qs = super(admin.ModelAdmin, self).get_queryset(request)


admin.site.register(Category)
admin.site.register(Articles, EditorAdmin)

