from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext as _


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """Creates a user with given email and password."""
        if not email:
            raise ValueError(_('Users must have email address.'))
        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        """Creates a superuser with given email and password."""
        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_admin = True
        user.save(using=self._db)
        return user
