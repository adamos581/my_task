from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.utils import timezone



class Category(models.Model):

    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class Articles(models.Model):

    title = models.CharField(max_length=150)
    author = models.ForeignKey(User, related_name='article_posts', on_delete=models.PROTECT)
    category = models.ForeignKey(Category, related_name='article_category', on_delete=models.PROTECT, null=True)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    published = models.BooleanField(default=False)

    class Meta:
        ordering = ('-publish',)
        permissions = (("can_read_art", "Can read article"),
                        ("can_publish_art", "Can publish article"),)


    def __str__(self):
        return self.title


