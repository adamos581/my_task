# Generated by Django 2.0 on 2018-02-16 22:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
            ],
        ),
        migrations.AlterModelOptions(
            name='articles',
            options={'ordering': ('-publish',), 'permissions': (('can_read_art', 'Can read article'), ('can_publish_art', 'Can publish article'))},
        ),
        migrations.AddField(
            model_name='articles',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='article_category', to='articles.Category'),
        ),
    ]
